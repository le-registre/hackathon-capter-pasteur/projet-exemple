## Présentation

> Une présentation courte du projet.

## Note technique

### Quel dispositif technique utilise le projet ?

> Quels capteurs / Cartes d'extensions ?

### Quel matériel nécessite le projet ?

> Lister tout le matériel nécessaire à l'utilisation du dispositif.

### A quelle fréquence le dispositif envoie des données ?

> Décrire précisément à quel moment le dispositif envoie des données vers la base de données.

### Notice d'installation

> Lister les points important de l'installation du dispositif.

### Schéma électronique du dispositif

> Mettre une photo du schéma électronique si besoin.
